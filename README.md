Une librairie externe faite par Philip Laliberté-Duguay
pour le langage Eiffel.

Cette librairie externe permet de :
    - calculer la norme d'un vecteur (longueur d'un vecteur)
    - calculer la pente d'un vecteur
    - calculer le produit scalaire entre deux vecteurs
    - convertir un angle de radian à degré
    - convertir un angle de degré à radian
    - calculer l'angle entre deux vecteurs