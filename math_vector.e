note
	description: "Classe permettant de faire des calculs sur les vecteurs et leurs composants."
	author: "Philip Lalibert�-Duguay"
	date: "Mardi, 26 Mars 2019"
	revision: "2.5"

class
	MATH_VECTOR

inherit
	DOUBLE_MATH

create
	default_create

feature --Acc�s
	calculate_vector_norm(a_x1,a_y1,a_x2,a_y2:INTEGER):INTEGER
		--Calcul de la norme d'un vecteur
		--Re�oit les coordonn�es x et y des deux extr�mit�es du vecteur � calculer.
		--Retourne la norme du vecteur
		require
			x1_valid:a_x1>=0
			y1_valid:a_y1>=0
			x2_valid:a_x2>=0
			y2_valid:a_y2>=0
		local
			l_calcul:INTEGER
			l_result:REAL_64
			l_conversion:INTEGER
			l_x:INTEGER
			l_y:INTEGER
		do
			l_x:=a_x2-a_x1
			l_y:=a_y2-a_y1
			l_calcul:=(l_x*l_x)+(l_y*l_y)
			l_result:=sqrt(l_calcul)
			l_conversion := l_result.rounded
			Result := l_conversion
		end

	calculate_vector_slope(a_x1,a_y1,a_x2,a_y2:INTEGER):REAL_64
		-- Calcul de la pente d'un vecteur
		-- Re�oit les coordonn�es x et y des deux extr�mit�es du vecteur � calculer
		-- Retourne la pente du vecteur en entier
		require
			x_valid_1:a_x1>0 or a_x2>0
			x_valid_2:a_x1>=0 and a_x2>=0
			y1_valid:a_y1>=0
			y2_valid:a_y2>=0
		do
			Result:= (a_y2-a_y1)/(a_x2-a_x1)
		end

	calculate_2_vectors_scalar_product(a_vector1_x1,a_vector1_y1,a_vector1_x2,a_vector1_y2,a_vector2_x1,a_vector2_y1,a_vector2_x2,a_vector2_y2:INTEGER):INTEGER
		-- Fait le produit scalaire de 2 vecteurs
		-- Re�oit les coordonn�es x et y des deux extr�mit�es des 2 vecteurs � calculer
		-- Retourne la r�ponse du produit scalaire
		require
			a_v1x1_valid:a_vector1_x1>=0
			a_v1y1_valid:a_vector1_y1>=0
			a_v1x2_valid:a_vector1_x2>=0
			a_v1y2_valid:a_vector1_y2>=0
			a_v2x1_valid:a_vector2_x1>=0
			a_v2y1_valid:a_vector2_y1>=0
			a_v2x2_valid:a_vector2_x2>=0
			a_v2y2_valid:a_vector2_y2>=0
		local
			l_vector1_u1:INTEGER
			l_vector1_u2:INTEGER
			l_vector2_v1:INTEGER
			l_vector2_v2:INTEGER
		do
			l_vector1_u1:=(a_vector1_x2-a_vector1_x1)
			l_vector1_u2:=(a_vector1_y2-a_vector1_y1)
			l_vector2_v1:=(a_vector2_x2-a_vector2_x1)
			l_vector2_v2:=(a_vector2_y2-a_vector2_y1)
			Result := (l_vector1_u1*l_vector2_v1)+(l_vector1_u2*l_vector2_v2)
		end

	convert_rad_to_degree(a_angle:REAL_64):INTEGER
		-- Converti un angle de radian � degr�.
		-- Re�oit `a_angle' qui est l'angle en radian
		-- Retourne l'angle en degr�
		local
			l_result:REAL_64
			l_conversion:INTEGER
		do
			l_result:=(a_angle * 180)/pi
			l_conversion:= l_result.rounded
			Result := l_conversion
		end

	convert_degree_to_rad(a_angle:INTEGER):REAL_64
		-- Converti un angle de degr� � radian.
		-- Re�oit `a_angle' qui est l'angle en degr�
		-- Retourne l'angle en radian
		do
			Result := (a_angle * pi)/180
		end

	calculate_angle_between_vectors(a_norm1,a_norm2,a_scalar_product:INTEGER):REAL_64
		-- Calcul l'angle entre 2 vecteurs
		-- Re�oit les normes `a_norme1',`a_norme2' qui sont les normes des deux vecteurs et le produit scalaire `a_scalar_product' des deux vecteurs
		-- Retourne l'angle entre les deux vecteurs en radian
		require
			a_norm_valid:(a_norm1>2 or a_norm2>2) or (a_norm1<-2 or a_norm2<-2) or (a_norm1=0 and a_norm2=0)
			a_scalar_product_valid:a_scalar_product>=3 or a_scalar_product<=-3 or a_scalar_product=0
		local
			l_calcul:REAL_64
		do
			if (a_scalar_product=0 and (a_norm1*a_norm2)=0) then
				Result := arc_cosine(0)
			else
				l_calcul:=(a_scalar_product)/(a_norm1*a_norm2)
				Result := arc_cosine(l_calcul)
			end
		end

note
	copyright: "Copyright (c) 2020, Philip Lalibert�-Duguay"
	license:   " MIT "

end
